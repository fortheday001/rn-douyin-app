import React from 'react';

// 引入导航器
import {createStackNavigator} from '@react-navigation/stack';

// 引入我的一套页面
import MePage from './Me';
import OrderPage from './Order';
import ProfilePage from './Profile';

// 创建栈导航器组件
const MeNavigator = createStackNavigator();

const Index = () => {
  return (
    <MeNavigator.Navigator>
      <MeNavigator.Screen
        name="Me-Me"
        component={MePage}
        options={{
          title: '',
          headerTransparent: true,
        }}
      />
      <MeNavigator.Screen
        name="Me-Order"
        component={OrderPage}
        options={{
          title: '我的订单',
        }}
      />
      <MeNavigator.Screen
        name="Me-Profile"
        component={ProfilePage}
        options={{
          title: '编辑资料',
        }}
      />
    </MeNavigator.Navigator>
  );
};

export default Index;
