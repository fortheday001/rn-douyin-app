import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Animated,
  FlatList,
  Button,
} from 'react-native';
import Avatar from '../../../components/avatar/Avatar';
import Drawer from '../../../components/me/Drawer';
import ViewPager from '@react-native-community/viewpager';
import TextButton from '../../../components/button/TextButton';
import IconButton from '../../../components/button/IconButton';

import OpenDrawerContext from '../../../context/OpenDrawerContext';

const Me = () => {
  const openDrawer = React.useContext(OpenDrawerContext);

  // 滑动框的引用
  const viewRef = React.useRef();

  // 当前被选中的框的下标
  const [activeIndex, setActiveIndex] = React.useState(0);

  // 定义动画变量
  const [move] = React.useState(new Animated.Value(0));

  // 监听当前下标，只要下标改变就执行动画
  React.useEffect(() => {
    Animated.timing(move, {
      toValue: activeIndex, // 把线滚动到当前下标的位置
      duration: 100,
      useNativeDriver: false,
    }).start();
  }, [activeIndex, move]);

  return (
    <View style={{flex: 1, backgroundColor: '#161721'}}>
      {/* 顶部背景图 */}
      <ImageBackground
        style={{
          width: '100%',
          height: 180,
        }}
        source={{
          uri:
            'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1588648423661&di=fe53fe107eea7c6af1e8282ffbded8e9&imgtype=0&src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2Fc2aedbbd216d58453c1b8e0176714cee0d9b5fa11a61c-O1FhqH_fw658',
        }}
      />

      {/* 头像、编辑资料、好友 */}
      <View
        style={{
          flexDirection: 'row',
          paddingLeft: 5,
          paddingRight: 5,
        }}>
        <View style={{top: -20}}>
          <Avatar
            border={5}
            size="big"
            uri="http://b-ssl.duitang.com/uploads/item/201706/27/20170627012435_mJLiX.thumb.700_0.jpeg"
          />
        </View>
        <Text style={[styles.txtBtn, {flex: 1}]}>编辑资料</Text>
        <Text style={styles.txtBtn}>+好友</Text>
      </View>

      {/* 三列切换：作品、动态、喜欢 */}
      <View
        style={{
          flexDirection: 'row',
        }}>
        <TextButton
          title="作品"
          style={[
            styles.viewerBtns,
            activeIndex === 0 ? styles.viewerBtnsActive : '',
          ]}
          onPress={() => {
            viewRef.current.setPage(0);
            setActiveIndex(0);
          }}
        />
        <TextButton
          title="动态"
          style={[
            styles.viewerBtns,
            activeIndex === 1 ? styles.viewerBtnsActive : '',
          ]}
          onPress={() => {
            // 切换到第二个框
            viewRef.current.setPage(1);
            setActiveIndex(1);
          }}
        />
        <TextButton
          title="喜欢"
          style={[
            styles.viewerBtns,
            activeIndex === 2 ? styles.viewerBtnsActive : '',
          ]}
          onPress={() => {
            viewRef.current.setPage(2);
            setActiveIndex(2);
          }}
        />
        {/* 黄线 */}
        <Animated.View
          style={{
            backgroundColor: '#F6D15B',
            height: 3,
            width: '33.333333%',
            position: 'absolute',
            bottom: 0,
            left: move.interpolate({
              inputRange: [0, 1, 2],
              outputRange: ['0%', '33.33333%', '66.66666%'],
            }),
          }}
        />
      </View>
      <ViewPager
        // 当滑动之后触发
        onPageSelected={(e) => {
          // 获取滑动之后的下标 ，并更新变量
          setActiveIndex(e.nativeEvent.position);
        }}
        ref={viewRef}
        initialPage={0}
        style={{
          backgroundColor: 'red',
          flex: 1,
        }}>
        <View key="works">
          <FlatList
            data={[1, 2, 3, 4, 5, 6]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={() => {
              return (
                <ImageBackground
                  style={{
                    flex: 1,
                    height: 200,
                    borderWidth: 1,
                  }}
                  source={{
                    uri:
                      'https://img11.360buyimg.com/mobilecms/s280x280_jfs/t1/117482/14/3661/100368/5ea91fbbE56b9645c/6a91d9e4f5de0db9.jpg.webp',
                  }}>
                  <Text>61</Text>
                </ImageBackground>
              );
            }}
            numColumns={3}
          />
        </View>
        <View key="news">
          <FlatList
            data={[1, 2, 3, 4, 5, 6]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={() => {
              return (
                <ImageBackground
                  style={{
                    flex: 1,
                    height: 200,
                    borderWidth: 1,
                  }}
                  source={{
                    uri:
                      'https://img11.360buyimg.com/mobilecms/s280x280_jfs/t1/117482/14/3661/100368/5ea91fbbE56b9645c/6a91d9e4f5de0db9.jpg.webp',
                  }}>
                  <Text>61</Text>
                </ImageBackground>
              );
            }}
            numColumns={3}
          />
        </View>
        <View key="like">
          <FlatList
            data={[1, 2, 3, 4, 5, 6]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={() => {
              return (
                <ImageBackground
                  style={{
                    flex: 1,
                    height: 200,
                    borderWidth: 1,
                  }}
                  source={{
                    uri:
                      'https://img11.360buyimg.com/mobilecms/s280x280_jfs/t1/117482/14/3661/100368/5ea91fbbE56b9645c/6a91d9e4f5de0db9.jpg.webp',
                  }}>
                  <Text>61</Text>
                </ImageBackground>
              );
            }}
            numColumns={3}
          />
        </View>
      </ViewPager>

      <View
        style={{
          position: 'absolute',
          top: 50,
          right: 20,
          backgroundColor: 'black',
          borderRadius: 16,
          padding: 4,
        }}>
        <IconButton
          name="apps"
          color="white"
          size={25}
          onPress={() => {
            openDrawer();
          }}
        />
      </View>
    </View>
  );
};

export default Me;

const styles = StyleSheet.create({
  txtBtn: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    backgroundColor: '#666',
    textAlign: 'center',
    textAlignVertical: 'center',
    marginLeft: 10,
    height: 45,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 3,
    marginTop: 10,
  },
  viewerBtns: {
    color: 'gray',
    fontSize: 16,
    flex: 1,
    textAlign: 'center',
    paddingBottom: 10,
  },
  viewerBtnsActive: {
    fontWeight: 'bold',
    color: 'white',
  },
});
