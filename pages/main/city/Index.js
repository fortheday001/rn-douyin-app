import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

// 引入栈导航器
import {createStackNavigator} from '@react-navigation/stack';

// 引入页面
import CityPage from './City'; // 列表页
import DetailPage from './Detail';

// 创建导航器
const CityNavigator = createStackNavigator();

const Index = () => {
  return (
    <CityNavigator.Navigator>
      {/* 列表 */}
      <CityNavigator.Screen
        name="City-City"
        component={CityPage}
        options={{
          headerStyle: {
            backgroundColor: '#161824',
          },
          headerTitleStyle: {
            color: 'white',
          },
          headerTitleAlign: 'center',
        }}
      />
      {/* 详情页 */}
      <CityNavigator.Screen
        name="City-Detail"
        component={DetailPage}
        options={{
          title: '',
          headerTransparent: true, // 设置标题栏为透明
          headerBackImage: () => (
            <Icon name="chevron-left" color="white" size={40} />
          ),
          headerRight: () => <Icon name="search" color="white" size={30} />,
        }}
      />
    </CityNavigator.Navigator>
  );
};

export default Index;
