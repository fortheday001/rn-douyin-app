import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  FlatList,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';
import Avatar from '../../../components/avatar/Avatar';
import Icon from 'react-native-vector-icons/MaterialIcons';

// 当前是否正在加载中
let loading = false;

const City = ({navigation}) => {
  // 当用户一打开这个页面，就执行获取所在城市并设置标题
  useEffect(() => {
    // navigator.geolocation.getCurrentPosition((res) => {
    //   console.log(res);
    // });
    // 1. 先获取用户所在的城市(这里先模拟写成沭阳)
    let city = '沭阳';
    // 2. 把名称设置为标题
    navigation.setOptions({
      title: city,
    });
  }, [navigation]);

  // 模拟数据
  const [data, setData] = useState([1, 2, 3, 4]);

  // 是否显示刷新的图标
  const [refreshing, setRefreshing] = useState(false);

  return (
    <View>
      <StatusBar barStyle="light-content" />
      <FlatList
        numColumns={2}
        data={data}
        renderItem={({item, index}) => <VideoItem navigation={navigation} />}
        keyExtractor={(item, index) => index.toString()}
        refreshing={refreshing}
        onRefresh={() => {
          // 显示刷新图标
          setRefreshing(true);

          // 调用接口获取最新的数据（模拟一下调用接口）
          setTimeout(() => {
            // 更新数据
            setData([1, 2, 5, 6, 7, 8, 9]);

            // 更新完之后隐藏刷新图标
            setRefreshing(false);
          }, 2000);
        }}
        ListFooterComponent={
          <Text
            style={{
              height: 50,
              textAlign: 'center',
              textAlignVertical: 'center',
            }}>
            加载中...
          </Text>
        }
        onEndReachedThreshold={0.2}
        onEndReached={() => {
          // 如果当前正在加载中，那么就直接退出，不需要再加载
          if (loading) {
            return;
          }

          // 标记一下，当前正在加载
          loading = true;

          // 调用接口加载更多数据
          setTimeout(() => {
            // 调用接口获取数据，把数据追回到数据的后面
            setData([...data, ...[5, 6, 7, 9]]);

            // 标记为加载完，可以进行下次加载了
            loading = false;
          }, 2000);
        }}
      />
    </View>
  );
};

// 每条记录的样式
function VideoItem({navigation}) {
  return (
    <View style={styles.VideoItem}>
      {/* 背景图 */}
      <TouchableWithoutFeedback
        onPress={() => {
          // 跳转到详情页
          navigation.navigate('City-Detail', {id: 13});
        }}>
        <ImageBackground
          style={{
            height: 300,
          }}
          source={{
            uri:
              'https://img11.360buyimg.com/pop/s1180x940_jfs/t1/111007/11/3838/70315/5eaa83d5E0c967a7a/94ecaf7387f6736c.jpg.webp',
          }}>
          <View style={{position: 'absolute', left: 10, bottom: 10}}>
            <View style={{flexDirection: 'row'}}>
              <Icon name="add-location" color="white" size={20} />
              <Text style={{color: 'white'}}>0.5km</Text>
            </View>
          </View>
          <View style={{position: 'absolute', right: 10, bottom: 10}}>
            <Avatar uri="http://gss0.baidu.com/-Po3dSag_xI4khGko9WTAnF6hhy/zhidao/pic/item/50da81cb39dbb6fd418d16c70b24ab18962b3778.jpg" />
          </View>
        </ImageBackground>
      </TouchableWithoutFeedback>
      {/* 标题 */}
      <Text>注高保真交互原型设计，喜欢研究各种体验友好的交互</Text>
    </View>
  );
}

export default City;

const styles = StyleSheet.create({
  VideoItem: {
    flex: 1,
    borderWidth: 3,
  },
});
