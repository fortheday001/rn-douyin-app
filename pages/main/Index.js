import React from 'react';
import {View, Alert} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import IconButton from '../../components/button/IconButton';
import ImagePicker from 'react-native-image-crop-picker';

// 创建底部导航器
const IndexBottomNavigator = createBottomTabNavigator();
// 导入四个页面
import IndexPage from './index/Index';
import CityPage from './city/Index';
import MessagePage from './message/Index';
import MePage from './me/Index';

const Index = () => {
  return (
    <>
      <IndexBottomNavigator.Navigator
        tabBarOptions={{
          showIcon: false, // 没有图标
          activeTintColor: 'white', // 选中时字体颜色
          labelStyle: {
            // 字体样式
            fontSize: 18,
            fontWeight: 'bold',
          },
          style: {
            backgroundColor: 'black',
          },
        }}>
        <IndexBottomNavigator.Screen
          name="Main"
          component={IndexPage}
          options={{title: '首页'}}
        />
        <IndexBottomNavigator.Screen
          name="City"
          component={CityPage}
          options={{title: '城市'}}
        />
        <IndexBottomNavigator.Screen
          name="Upload"
          component={CityPage}
          options={{title: '+'}}
        />
        <IndexBottomNavigator.Screen
          name="Message"
          component={MessagePage}
          options={{title: '消息'}}
        />
        <IndexBottomNavigator.Screen
          name="Me"
          component={MePage}
          options={{title: '我'}}
        />
      </IndexBottomNavigator.Navigator>
      <View
        style={{
          alignItems: 'center',
        }}>
        <View
          style={{
            position: 'absolute',
            bottom: 0,
            backgroundColor: 'white',
            paddingLeft: 8,
            paddingRight: 8,
            borderRadius: 5,
            borderLeftWidth: 2,
            borderLeftColor: '#78E5E7',
            borderRightWidth: 2,
            borderRightColor: '#E54667',
          }}>
          <IconButton
            name="add"
            onPress={() => {
              ImagePicker.openCamera({
                mediaType: 'video',
              })
                .then((img) => {
                  console.log(img);
                })
                .catch((v) => {
                  console.log(v);
                });
            }}
          />
        </View>
      </View>
    </>
  );
};

export default Index;
