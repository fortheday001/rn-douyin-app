import React from 'react';
import {
  StyleSheet,
  View,
  Animated,
  Dimensions,
  FlatList,
  Text,
} from 'react-native';
import VideoList from '../../../components/video/VideoList';
import ViewPager from '@react-native-community/viewpager';
import IconButton from '../../../components/button/IconButton';
import TextButton from '../../../components/button/TextButton';
import CommentItem from '../../../components/comment/CommentItem';
import Panel from '../../../components/common/Panel';

import OpenCommentContext from '../../../context/OpenCommentContext';

const {width} = Dimensions.get('window');

// 创建一个 ref
const viewPageRef = React.createRef();

const Video = () => {
  // 白线左边的位置
  const [lineLeft] = React.useState(new Animated.Value(0));

  // 评论框是否打开
  const [openComment, setOpenComment] = React.useState(false);

  return (
    <>
      <OpenCommentContext.Provider value={setOpenComment}>
        {/* 视频列表 */}
        <ViewPager
          // 每次滑动完之后执行的事件
          onPageSelected={(e) => {
            // 根据当前的下标计算出移动之后的位置
            let toValue = e.nativeEvent.position === 0 ? 0 : 55;
            // 白线移动到目标位置
            // 白线移动的动画
            Animated.timing(lineLeft, {
              toValue, // 移动的位置
              duration: 200, // 0.2s
              useNativeDriver: true, // 是否启用原生动画驱动（left动画不支持原生）
            }).start();
          }}
          ref={viewPageRef}
          style={styles.container}
          initialPage={0}>
          {/* 关注 */}
          <View key="gz">
            <VideoList />
          </View>
          {/* 推荐 */}
          <View key="tj">
            <VideoList />
          </View>
        </ViewPager>
      </OpenCommentContext.Provider>

      {/* 按钮列表 */}
      <View style={styles.btnList}>
        <IconButton name="live-tv" color="white" />
        {/* 两个文字按钮 */}
        <View style={styles.textBtnList}>
          <View
            style={{
              flexDirection: 'row',
              paddingLeft: 0,
            }}>
            <TextButton
              style={[
                styles.txtBtnLabel,
                {
                  paddingRight: '5%',
                },
              ]}
              title="关注"
              onPress={() => {
                // 切换屏幕内容
                viewPageRef.current.setPage(0);
                // 白线移动的动画
                Animated.timing(lineLeft, {
                  toValue: 0,
                  duration: 200, // 0.2s
                  useNativeDriver: true, // 是否启用原生动画驱动（left动画不支持原生）
                }).start();
              }}
            />
            <TextButton
              style={styles.txtBtnLabel}
              title="推荐"
              onPress={() => {
                // 切换屏幕内容
                viewPageRef.current.setPage(1);
                // 白线移动的动画
                Animated.timing(lineLeft, {
                  toValue: 55,
                  duration: 200, // 0.2s
                  useNativeDriver: true, // 是否启用原生动画驱动（left动画不支持原生）
                }).start();
              }}
            />
            <Animated.View
              style={[
                styles.line,
                {
                  transform: [
                    {
                      translateX: lineLeft,
                    },
                  ],
                },
              ]}
            />
          </View>
        </View>
        <IconButton name="search" color="white" />
      </View>

      {/* 评论 */}
      <Panel
        open={openComment}
        onClose={() => {
          setOpenComment(false);
        }}>
        <FlatList
          data={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]}
          keyExtractor={(item) => item.toString()}
          renderItem={() => <CommentItem />}
        />
      </Panel>
    </>
  );
};

export default Video;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnList: {
    paddingLeft: 10,
    paddingRight: 10,
    position: 'absolute',
    top: 30,
    flexDirection: 'row', // 子元素横过来
  },
  textBtnList: {
    flex: 1, // 占满所有可用空间
    flexDirection: 'row', // 子元素横向排列
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtBtnLabel: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  line: {
    backgroundColor: 'white',
    height: 5,
    width: 35,
    position: 'absolute',
    left: 0,
    bottom: -5,
  },
});
