import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import TextButton from '../button/TextButton';
import {useNavigation} from '@react-navigation/native';

// 当前是展开还是折叠
let isopen = false;

const Menu = () => {
  // 生成导航器对象
  // const navigation = useNavigation();

  // 控制动画的变量
  const [move] = React.useState(new Animated.Value(0));

  return (
    <View style={styles.container}>
      <MenuItem icon="account-balance" title="个人中心" />

      <Animated.View
        style={{
          zIndex: 9999,
          overflow: 'hidden',
          height: move.interpolate({
            inputRange: [0, 1],
            outputRange: [40, 170],
          }),
        }}>
        {/* 订单按钮 */}
        <MenuItem
          icon="bluetooth"
          title="订单"
          onPress={() => {
            console.log('hello');

            // 创建动画并执行
            Animated.timing(move, {
              toValue: isopen ? 1 : 0,
              duration: 100,
              useNativeDriver: false, // 是否启用原生驱动，因为 height 不支持，所以只能是 false
            }).start(() => {
              // 当动画执行完之后，修改折叠展开的状态
              isopen = !isopen;
            });
          }}
        />
        {/* 三角形图标 */}
        <Animated.View
          style={{
            position: 'absolute',
            right: 10,
            top: 10,
            transform: [
              {
                rotate: move.interpolate({
                  inputRange: [0, 1],
                  outputRange: ['0deg', '180deg'],
                }),
              },
            ],
          }}>
          <Tri />
        </Animated.View>
        {/* 二级按钮 */}
        <TextButton
          style={styles.subBtn}
          title="订单列表"
          onPress={() => {
            // navigation.navigate('Me-Order');
          }}
        />
        <TextButton style={styles.subBtn} title="购物指南" />
        <TextButton style={styles.subBtn} title="购物指南" />
        <TextButton style={styles.subBtn} title="购物指南" />
      </Animated.View>

      <MenuItem icon="brightness-high" title="设置" />
    </View>
  );
};

function MenuItem({icon, title, onPress}) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.MenuItem}>
        <Icon color="white" name={icon} size={22} />
        <Text
          style={{
            color: 'white',
            fontSize: 18,
            marginLeft: 8,
          }}>
          {title}
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
}

function Tri() {
  return <View style={styles.tri} />;
}

export default Menu;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    paddingLeft: 20,
  },
  MenuItem: {
    flexDirection: 'row',
    paddingTop: 8,
    paddingBottom: 8,
  },
  tri: {
    width: 0,
    height: 0,
    borderTopColor: 'gray',
    borderBottomColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderWidth: 10,
    borderRadius: 4,
    top: '25%', // 让三角形居中
  },
  subBtn: {
    color: 'white',
    paddingLeft: 30,
    fontSize: 16,
    paddingTop: 5,
    paddingBottom: 5,
  },
});
