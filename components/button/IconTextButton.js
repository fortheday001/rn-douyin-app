import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import IconButton from './IconButton';

const IconTextButton = ({name, text, onPress, color, size}) => {
  return (
    <>
      <IconButton name={name} onPress={onPress} color={color} size={size} />
      <Text
        style={{
          color: 'white',
          fontWeight: 'bold',
          fontSize: 16,
        }}>
        {text}
      </Text>
    </>
  );
};

IconTextButton.defaultProps = {
  color: 'white',
  size: 40,
};

export default IconTextButton;

const styles = StyleSheet.create({});
