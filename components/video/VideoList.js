import React from 'react';
import {View, FlatList, Dimensions} from 'react-native';
import VideoPlay from './VideoPlay';

// 获取屏幕的高度
const {height} = Dimensions.get('window');

// 模拟一个数据（真实的视频数据应该是从接口获取的）
const data = [1, 2];

const VideoList = () => {
  return (
    <FlatList
      pagingEnabled={true} // 一次滑动一屏
      data={data}
      renderItem={() => (
        <View style={{height: height - 50}}>
          <VideoPlay />
        </View>
      )}
      keyExtractor={(item, index) => index.toString()}
    />
  );
};

export default VideoList;
