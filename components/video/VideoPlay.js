import React from 'react';
import {StyleSheet, View, StatusBar} from 'react-native';
import Video from 'react-native-video';
import Avatar from '../avatar/Avatar';
import HeartBtn from '../button/HeartBtn';
import RotateAvatar from '../avatar/RotateAvatar';
import IconTextButton from '../button/IconTextButton';

import OpenCommentContext from '../../context/OpenCommentContext';

const VideoPlay = () => {
  const openComment = React.useContext(OpenCommentContext);

  return (
    <View style={{flex: 1}}>
      <StatusBar
        translucent={true}
        backgroundColor="rgba(0,0,0,0)"
        barStyle="dark-content"
      />
      {/* 视频播放 */}
      <Video
        resizeMode="cover"
        style={styles.video}
        // source={{uri: 'http://player.alicdn.com/video/aliyunmedia.mp4'}}
        poster="https://img11.360buyimg.com/da/s1180x940_jfs/t1/101906/25/19268/97220/5ea2ae92Ee1876bdb/44f7265ea3518223.jpg.webp"
        posterResizeMode="cover"
        controls={true}
        paused={true}
      />
      {/* 图标按钮 */}
      <View style={styles.btns}>
        <View style={styles.btnItem}>
          <Avatar uri="http://5b0988e595225.cdn.sohucs.com/images/20170724/ad28da0d658b43aba84ce91df9cacdad.jpeg" />
        </View>
        <View style={styles.btnItem}>
          <HeartBtn number="23.3w" />
        </View>
        <View style={styles.btnItem}>
          <IconTextButton
            name="comment"
            text="9458"
            onPress={() => {
              // 打开评论框
              openComment(true);
            }}
          />
        </View>
        <View style={styles.btnItem}>
          <IconTextButton name="share" text="4354" />
        </View>
        <View style={styles.btnItem}>
          <RotateAvatar uri="http://5b0988e595225.cdn.sohucs.com/images/20170724/ad28da0d658b43aba84ce91df9cacdad.jpeg" />
        </View>
      </View>
    </View>
  );
};

export default VideoPlay;

const styles = StyleSheet.create({
  video: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
  },
  btns: {
    position: 'absolute',
    right: 10,
    bottom: 10,
    alignItems: 'center',
  },
  btnItem: {
    marginTop: 20,
  },
});
