import React from 'react';
import {Image} from 'react-native';

const Avatar = ({uri, size, border, borderColor}) => {
  return (
    <Image
      source={{uri}}
      style={{
        borderColor,
        borderWidth: border,
        borderRadius: size === 'big' ? 50 : 25,
        width: size === 'big' ? 100 : 50,
        height: size === 'big' ? 100 : 50,
      }}
    />
  );
};

Avatar.defaultProps = {
  size: 'sm',
  border: 0,
  borderColor: 'black',
};

export default Avatar;
