import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Avatar from '../avatar/Avatar';
import HeartBtn from '../button/HeartBtn';

const CommentItem = () => {
  return (
    <View style={styles.commentItem}>
      {/* 头像 */}
      <Avatar uri="http://5b0988e595225.cdn.sohucs.com/images/20170724/ad28da0d658b43aba84ce91df9cacdad.jpeg" />
      {/* 评论的内容 */}
      <View style={{flex: 1, paddingLeft: 10, paddingRight: 10}}>
        <Text style={{color: 'white', fontWeight: 'bold'}}>山岗先生</Text>
        <Text style={{color: 'white'}}>
          还不错还不错还不错还不错还不错还不错还不错还不错还不错还不错还不错
        </Text>
      </View>
      {/* 心 */}
      <HeartBtn number="23.3w" />
    </View>
  );
};

export default CommentItem;

const styles = StyleSheet.create({
  commentItem: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
  },
});
