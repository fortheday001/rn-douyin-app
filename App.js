import React from 'react';
import {
  View,
  Dimensions,
  Animated,
  Button,
  PanResponder,
  SafeAreaView,
  TouchableWithoutFeedback,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Menu from './components/me/Menu';

import OpenDrawerContext from './context/OpenDrawerContext';

// 获取屏幕的宽度
const {width} = Dimensions.get('window');
const moveLeftPos = -1 * width * 0.75;

// 创建 Stack 导航器
const SplashNavigator = createStackNavigator();
// 管理封面页和主页
import SplashPage from './pages/Splash'; // 封面页
import IndexPage from './pages/main/Index'; // 主页

const App = () => {
  // 定义整体移动动画的变量
  const [move] = React.useState(new Animated.Value(0));

  // 定义一个状态数据，用来控制这个层的显示和隐藏
  const [open, setOpen] = React.useState(false);

  const [movePan, setMovePan] = React.useState({});

  // 打开抽屉页函数
  const openDrawer = React.useCallback(() => {
    setOpen(true);
    // 执行动画
    Animated.timing(move, {
      toValue: 1,
      duration: 100,
      useNativeDriver: true,
    }).start();
  }, [move]);

  React.useEffect(() => {
    let pan = PanResponder.create({
      // 开启手势滑动功能
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,

      // 不需要获取其他点击等事件
      // onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      // onStartShouldSetPanResponderCapture: (evt, gestureState) => true,

      // 滑动结束时触发
      // gestureState.dx : 结束的坐标-开始的坐标 （正数：向右滑动的距离 ，负数：向左滑动的距离）
      // gestureState.dy : 结束的坐标-开始的坐标 （正数：向下滑动的距离 ，负数：向上滑动的距离）
      onPanResponderMove: (evt, gestureState) => {
        // 当向右滑动了 80 时执行
        if (gestureState.dx > 80) {
          // 执行动画
          Animated.timing(move, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true,
          }).start(() => {
            // 当动画执行完之后隐藏层
            setOpen(false);
          });
        }
      },
    });

    setMovePan(pan);
  }, [move]);

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1}}>
        {/* 主APP */}
        <Animated.View
          style={{
            flex: 1,
            transform: [
              {
                translateX: move.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, moveLeftPos],
                }),
              },
            ],
          }}>
          {/* 把 openDrawer 这个函数传递给所有子组件 */}
          <OpenDrawerContext.Provider value={openDrawer}>
            {/* 子页面 */}
            <NavigationContainer>
              <SplashNavigator.Navigator headerMode="none">
                {/* 封面页 */}
                {/* <SplashNavigator.Screen name="Splash" component={SplashPage} /> */}
                {/* 底部导航器 */}
                <SplashNavigator.Screen name="Index" component={IndexPage} />
              </SplashNavigator.Navigator>
            </NavigationContainer>
          </OpenDrawerContext.Provider>
          {/* 把整个 APP 盖上的层
        当 open 为 true 时显示组件
      */}
          {open && (
            <TouchableWithoutFeedback
              onPress={() => {
                // 执行动画
                Animated.timing(move, {
                  toValue: 0,
                  duration: 100,
                  useNativeDriver: true,
                }).start(() => {
                  // 当动画执行完之后隐藏层
                  setOpen(false);
                });
              }}>
              <View
                style={{
                  position: 'absolute',
                  width: '100%',
                  height: '100%',
                  left: 0,
                  top: 0,
                  backgroundColor: 'rgba(0,0,0,0.7)',
                }}
              />
            </TouchableWithoutFeedback>
          )}
        </Animated.View>
        {/* 抽屉导航上的按钮 */}
        <Animated.View
          {...movePan.panHandlers}
          style={{
            position: 'absolute',
            right: '-75%',
            transform: [
              {
                translateX: move.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, moveLeftPos],
                }),
              },
            ],
            top: 0,
            width: '75%',
            height: '100%',
            backgroundColor: 'black',
          }}>
          <Menu />
        </Animated.View>
      </View>
    </SafeAreaView>
  );
};

export default App;
